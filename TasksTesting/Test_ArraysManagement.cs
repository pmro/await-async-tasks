using NUnit.Framework;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using System.Threading;
using Await_Async_Tasks;

namespace TasksTesting
{
    /// <summary>
    /// Class Tests <see cref="ArraysManagement"/>.
    /// </summary>
    public class Test_ArraysManagement
    {
        /// <summary>
        /// Create Array with random values on given valid count value and ranges should succeed.
        /// </summary>
        /// <param name="count">Number of elements for array.</param>
        /// <param name="rangeMin">Min possible element value.</param>
        /// <param name="rangeMax">Max possible element value.</param>
        [TestCase(10, -100, 100)]
        [TestCase(10, 0, 1000)]
        [TestCase(10, -1000, 10)]
        public void CreateArrayShouldReturnNotEmptyArray(int count, int rangeMin, int rangeMax)
        {
            var assertArray = ArraysManagement.CreateArrayWithRandomValues(count, rangeMin, rangeMax, tokenSource);

            Assert.NotNull(assertArray);
            Assert.IsNotEmpty(assertArray);
            Assert.AreEqual(count, assertArray.Count);
        }

        /// <summary>
        /// Attempt to create array with invalid cunt value should throw exception.
        /// </summary>
        /// <param name="count"></param>
        [TestCase(0)]
        [TestCase(-1)]
        [TestCase(-10)]
        public void CreateArrayWithCountValueLessThanOneShouldThrowException(int count)
        {
            Assert.Throws<ArgumentException>(() => ArraysManagement.CreateArrayWithRandomValues(count, rangeMin, rangeMax, tokenSource));
        }

        /// <summary>
        /// Attemp to create array with invalid range - when range min value is greater or equal to range max value, should throw exception.
        /// </summary>
        /// <param name="rangeMin">Min possible element value.</param>
        /// <param name="rangeMax">Max possible element value.</param>
        [TestCase(100, 100)]
        [TestCase(100, 70)]
        public void CreateArrayWithMinRangeGreaterOrEqualToMaxRangeShouldThrowException(int rangeMin, int rangeMax)
        {
            Assert.Throws<ArgumentOutOfRangeException>(() => ArraysManagement.CreateArrayWithRandomValues(count, rangeMin, rangeMax, tokenSource));
        }

        /// <summary>
        /// Multiplyy array by random value - in valid ranges, should succeed.
        /// </summary>
        [Test]
        public void MultiplyArrayByRandomValueShouldSucceed()
        {
            List<int> arrayToCompare = new List<int>(originalArray);

            ArraysManagement.MultiplyArrayByRandomValue(originalArray, rangeMin, rangeMax, tokenSource);

            CollectionAssert.AreNotEqual(originalArray, arrayToCompare);
            Assert.AreNotEqual(arrayToCompare[0], originalArray[0]);
        }

        /// <summary>
        /// Attemp to multiply null or empty array should throw exception.
        /// </summary>
        [Test]
        public void MultiplyNullOrEmptyArrayShouldThrowException()
        {
            Assert.Throws<ArgumentNullException>(() => ArraysManagement.MultiplyArrayByRandomValue(null, rangeMin, rangeMax, tokenSource));
            Assert.Throws<ArgumentNullException>(() => ArraysManagement.MultiplyArrayByRandomValue(new List<int>(), rangeMin, rangeMax, tokenSource));
        }

        /// <summary>
        /// Attemp to multiply array with invalid range - when range min value is greater or equal to range max value, should throw exception.
        /// </summary>
        /// <param name="rangeMin">Min possible element value.</param>
        /// <param name="rangeMax">Max possible element value.</param>
        [TestCase(100, 100)]
        [TestCase(100, 70)]
        public void MultiplyArrayWithMinRangeGreaterOrEqualToMaxRangeShouldThrowException(int rangeMin, int rangeMax)
        {
            Assert.Throws<ArgumentOutOfRangeException>(() => ArraysManagement.MultiplyArrayByRandomValue(originalArray, rangeMin, rangeMax, tokenSource));
        }

        /// <summary>
        /// Sort not empty, nor null array should succeed.
        /// </summary>
        [Test]
        public void SortArrayByAscendingShouldSucceed()
        {
            ArraysManagement.SortArrayByAscending(originalArray, tokenSource);

            CollectionAssert.AreEqual(sortedOriginalArray, originalArray);
            CollectionAssert.IsOrdered(originalArray);
        }

        /// <summary>
        /// Attemp to sort null or empty array should throw exception.
        /// </summary>
        [Test]
        public void SortArrayByAscendingWithNullArrayParamShouldThrowException()
        {
            ArraysManagement.SortArrayByAscending(originalArray, tokenSource);

            Assert.Throws<ArgumentNullException>(() => ArraysManagement.SortArrayByAscending(null, tokenSource));
        }

        /// <summary>
        /// Calculate average value of array elements should succeed and return value.
        /// </summary>
        [Test]
        public void CalculateAverageValueShouldReturnValidValue()
        {
            long result = ArraysManagement.CalculateAverageValue(originalArray, tokenSource);

            Assert.AreEqual(originalAverageValue, result);
        }

        /// <summary>
        /// Attemp to calculate average value of null or empty array should throw exception.
        /// </summary>
        [Test]
        public void CalculateAverageValueOnNullOrEmptyArrayShouldThrowException()
        {
            ArraysManagement.CalculateAverageValue(originalArray, tokenSource);
            Assert.Throws<ArgumentNullException>(() => ArraysManagement.CalculateAverageValue(null, tokenSource));
            Assert.Throws<ArgumentNullException>(() => ArraysManagement.CalculateAverageValue(new List<int>(), tokenSource));
        }

        private List<int> originalArray;
        private readonly int originalAverageValue = 32;
        private List<int> sortedOriginalArray;
        private List<int> multiplyedArray;
        private List<int> sortedMultipliedArray;
        private readonly int count = 10;
        private int rangeMin;
        private int rangeMax;
        private CancellationTokenSource tokenSource;

        /// <summary>
        /// Setup properties and fields for tests.
        /// </summary>
        [SetUp]
        public void SetUp()
        {
            originalArray = new List<int>()
            {
                64, 4, 47, 98, 41, -18, 6, 1, -4, 89
            };

            sortedOriginalArray = new List<int>()
            {
                -18, -4, 1, 4, 6, 41, 47, 64, 89, 98
            };

            rangeMin = -100;
            rangeMax = 100;
            tokenSource = new CancellationTokenSource();

            multiplyedArray = new List<int>()
            {
                -5632, -352, -4135, -8624, -3608, 1584, -528, -88, 352, -7832
            };

            sortedMultipliedArray = new List<int>(multiplyedArray);
            sortedMultipliedArray.Sort();
        }
    }
}