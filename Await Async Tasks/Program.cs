﻿using System;
using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;

namespace Await_Async_Tasks
{
    public static class Program
    {
        static async Task Main()
        {
            Console.WriteLine("Hello Async Await Tasks!\n");

            int arrayCount = 10;
            int rangeMin = -100;
            int rangeMax = 100;

            using var cts = new CancellationTokenSource();
            CancellationToken token = cts.Token;

            Task<List<int>> createArray = Task<List<int>>.Run(() => {
                TasksManagement.ChecksCancelationAndThrowIfOccurs(token);
                return ArraysManagement.CreateArrayWithRandomValues(arrayCount, rangeMin, rangeMax, cts); 
            }, token);

            List<int> createResult = await createArray;

            Task<List<int>> multiplyArray = Task<List<int>>.Run(() => {
                TasksManagement.ChecksCancelationAndThrowIfOccurs(token);
                return ArraysManagement.MultiplyArrayByRandomValue(createResult, rangeMin, rangeMax, cts);
            }, token);

            List<int> multiplyResult = await multiplyArray;

            Task<List<int>> sortArray = Task<List<int>>.Run(() => {
                TasksManagement.ChecksCancelationAndThrowIfOccurs(token);
                return ArraysManagement.SortArrayByAscending(multiplyResult, cts);
            }, token);

            List<int> sortResult = await sortArray;

            Task<long> averageOfArrayElems = Task<long>.Run(() => {
                TasksManagement.ChecksCancelationAndThrowIfOccurs(token);
                return ArraysManagement.CalculateAverageValue(sortResult, cts);
            }, token);

            long averageResult = await averageOfArrayElems;

            await Task.Run(() => {
                TasksManagement.ChecksCancelationAndThrowIfOccurs(token);
                Console.WriteLine($"Calculated Average Value = {averageResult}\n");
            }, token);

            Task.WaitAll(createArray, multiplyArray, sortArray, averageOfArrayElems);
            Console.WriteLine("Create Array Status: {0}", createArray.Status);
            Console.WriteLine("Multiply Array Status: {0}", multiplyArray.Status);
            Console.WriteLine("Sort Array Status: {0}", sortArray.Status);
            Console.WriteLine("Average Value of Array Status: {0}", averageOfArrayElems.Status);
        }
    }
}
