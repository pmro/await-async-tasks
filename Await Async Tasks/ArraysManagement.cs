﻿using System;
using System.Collections.Generic;
using System.Threading;

namespace Await_Async_Tasks
{
    /// <summary>
    /// Class allows to invoke some actions on List<int> array.
    /// </summary>
    public static class ArraysManagement
    {
        private const string borderTypeMax = "MAX";
        private const string borderTypeMin = "MIN";
        
        /// <summary>
        /// Create int array with random values.
        /// </summary>
        /// <param name="count">Number of element should be added to array.</param>
        /// <param name="rangeMin">Minimal range value.</param>
        /// <param name="rangeMax">Maximal range value.</param>
        /// <returns>List with int values that are random in given ranges.</returns>
        /// <exception cref="ArgumentException">Occurs if count value is less than one.</exception>
        /// <exception cref="ArgumentOutOfRangeException">Occurs if rangeMin value is greater or equal to rangeMax.</exception>
        public static List<int> CreateArrayWithRandomValues(int count, int rangeMin, int rangeMax, CancellationTokenSource tokenSource)
        {
            if (count < 1)
            {
                tokenSource.Cancel();
                throw new ArgumentException("Impossible to create empty array with random values!", nameof(count));
            }

            ThrowExceptionForRangesIfOccurs(ref rangeMin, ref rangeMax, tokenSource);

            List<int> randoms = new List<int>(count);
            Random randomGenerator = new Random();

            while (randoms.Count < count)
            {
                randoms.Add(GetRandomNumber(randomGenerator, rangeMin, rangeMax));
            }

            Print(randoms, "Original Array");

            return randoms;
        }

        /// <summary>
        /// Muultiply array element by random value.
        /// </summary>
        /// <param name="array">Array to multiply.</param>
        /// <param name="rangeMin">Minimal range value for multiplier.</param>
        /// <param name="rangeMax">Maximal range value for multiplier.</param>
        /// <returns>List with int values that are multiplied by random value.</returns>
        /// <exception cref="ArgumentNullException">Occurs if given array is null or empty.</exception>
        /// <exception cref="ArgumentOutOfRangeException">Occurs if rangeMin value is greater or equal to rangeMax.</exception>
        public static List<int> MultiplyArrayByRandomValue(List<int> array, int rangeMin, int rangeMax, CancellationTokenSource tokenSource)
        {
            if (array is null || array.Count == 0)
            {
                tokenSource.Cancel();
                throw new ArgumentNullException(nameof(array), "Array should has any values!");
            }

            ThrowExceptionForRangesIfOccurs(ref rangeMin, ref rangeMax, tokenSource);

            Random randomGenerator = new Random();
            int randomValue = GetRandomNumber(randomGenerator, rangeMin, rangeMax);

            int arrayMaxValue = FindBorderValue(array, "MAX");
            long maxIntBorder = int.MaxValue;
            long maxAcceptableMultiplyingNumber = maxIntBorder / arrayMaxValue;

            int arrayMinValue = FindBorderValue(array, "MIN");
            long minIntBorder = int.MinValue;
            long minAcceptableMultiplyingNumber = Math.Abs(minIntBorder / arrayMinValue) * (-1);

            while (randomValue > maxAcceptableMultiplyingNumber || randomValue < minAcceptableMultiplyingNumber)
            {
                randomValue = GetRandomNumber(randomGenerator, rangeMin, rangeMax);
            }

            int index = 0;

            while (index < array.Count)
            {
                array[index] *= randomValue;
                index++;
            }

            Print(array, $"Multiplied Array By Random Value = {randomValue}");

            return array;
        }

        /// <summary>
        /// Sort array to Ascending.
        /// </summary>
        /// <param name="array">Array to sort.</param>
        /// <returns>Sorted ascending list.</returns>
        /// <exception cref="ArgumentNullException">Occurs if given array is null.</exception>
        public static List<int> SortArrayByAscending(List<int> array, CancellationTokenSource tokenSource)
        {
            if (array is null)
            {
                tokenSource.Cancel();
                throw new ArgumentNullException(nameof(array), "Imposible to sort null object.");
            }

            array.Sort();
            Print(array, "Sorted Array By Ascending");

            return array;
        }

        /// <summary>
        /// Calculate average value based on arrays values and elements number.
        /// </summary>
        /// <param name="array">Array as source.</param>
        /// <returns>Calculated average value.</returns>
        /// <exception cref="ArgumentNullException">Occurs if given array is null or empty.</exception>
        public static long CalculateAverageValue(List<int> array, CancellationTokenSource tokenSource)
        {
            if (array is null || array.Count == 0)
            {
                tokenSource.Cancel();
                throw new ArgumentNullException(nameof(array), "Impossible to calculate average value on null / empty source!");
            }

            long sum = 0;

            foreach (int number in array)
            {
                sum += number;
            }

            long result = sum / array.Count;
            
            return result;
        }

        private static void Print(List<int> array, string message)
        {
            int index = 0;
            Console.WriteLine(message);

            foreach (int number in array)
            {
                Console.WriteLine($"Index {index} -> {number}");
            }

            Console.WriteLine();
        }

        private static int FindBorderValue(List<int> array, string valueBorderType)
        {
            int value = array[0];

            foreach (int number in array)
            {
                if (valueBorderType == borderTypeMax && number > value)
                {
                    value = number;
                }
                else if(valueBorderType == borderTypeMin && number < value)
                {
                   value = number;
                }
            }

            return value;
        }

        private static int GetRandomNumber(Random randomGenerator, int min, int max)
        {
            return randomGenerator.Next(min, max);
        }

        private static void ThrowExceptionForRangesIfOccurs(ref int rangeMin, ref int rangeMax, CancellationTokenSource tokenSource)
        {
            if (rangeMin >= rangeMax)
            {
                tokenSource.Cancel();
                throw new ArgumentOutOfRangeException(nameof(rangeMin), "Range values are invalid, range min has to be lower value than range max.");
            }
        }
    }
}
