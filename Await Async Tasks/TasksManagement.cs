﻿using System;
using System.Threading;

namespace Await_Async_Tasks
{
    /// <summary>
    /// Class allows to invoke tasks with cancellation token.
    /// </summary>
    public static class TasksManagement
    {
        public static void ChecksCancelationAndThrowIfOccurs(CancellationToken token)
        {
            if (token.IsCancellationRequested)
            {
                Console.WriteLine("\nCancellation requested in continuation...\n");
                token.ThrowIfCancellationRequested();
            }
        }
    }
}
